"""empty message

Revision ID: 2d3ac455e294
Revises: 0e65683df81a
Create Date: 2020-07-27 03:31:02.257296

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2d3ac455e294'
down_revision = ('0e65683df81a')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
