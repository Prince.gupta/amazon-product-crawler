"""Create product table

Revision ID: 0e65683df81a
Revises: 88e9ee99b8c1
Create Date: 2020-07-27 01:23:01.881663

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0e65683df81a'
down_revision = '88e9ee99b8c1'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'products',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String),
        sa.Column('source_url', sa.Text),
        sa.Column('site_id', sa.Integer, sa.ForeignKey('sites.id')),
        sa.Column('category_id', sa.Integer, sa.ForeignKey('categories.id')),
        sa.Column('archived_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime,
                  onupdate=sa.func.now(), default=sa.func.now()),
        sa.Column('created_at', sa.DateTime, default=sa.func.now())
    )


def downgrade():
    op.drop_table('products')
