"""Create category table

Revision ID: 88e9ee99b8c1
Revises: 81eebef26aba
Create Date: 2020-07-27 01:22:46.124163

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '88e9ee99b8c1'
down_revision = '81eebef26aba'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'categories',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String),
        sa.Column('site_id', sa.Integer, sa.ForeignKey('sites.id')),
        sa.Column('archived_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime,
                  onupdate=sa.func.now(), default=sa.func.now()),
        sa.Column('created_at', sa.DateTime, default=sa.func.now())
    )


def downgrade():
    op.drop_table('categories')
