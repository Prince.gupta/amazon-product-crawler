from googlesearch import search
import requests
from bs4 import BeautifulSoup
import re


class GoogleSearch:
    def __init__(self, query):
        self.query = query + ' flipkart'

    def google_search(self):
        query = self.query
        results = []
        for result in search(query, tld="co.in", num=5, stop=5, pause=1):
            if 'flipkart' in result:
                results.append(result)
        return results[0]
