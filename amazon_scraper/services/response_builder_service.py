import json
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, subqueryload
import projectconfig
from models.sites import Site
from models.categories import Category
from models.products import Product
from models.metadata import Metadata
from models.reviews import Review
from models.product_meta import ProductMeta
from dateutil.parser import parse
db_engine = create_engine(projectconfig.db_url, pool_size=80, max_overflow=0)
Session = sessionmaker(bind=db_engine)
session = Session()


class ResponseBuilder:
    def __init__(self, item_dict):
        self.item_dict = item_dict

    def format_api_response(self):
        item_dict = self.item_dict

        def _product_name_resolver(product_dict):
            return product_dict.get('product_name')

        def _product_meta_resolver(item_dict, product_name):
            products = session.query(Product).filter_by(name=product_name, archived_at=None).all()
            meta_dict = {}
            category_collection = []
            details_meta_dict = {}

            def _review_resolver(reviews):
                review_collection = []
                for review in reviews:
                    review_dict = {
                        "author_name": review.name,
                        "author_path": review.author_path,
                        "review_title": review.review_title,
                        "review": review.review,
                        "verified_purchase": review.verified_purchase,
                        "review_rating": review.review_rating
                    }
                    review_collection.append(review_dict)
                return review_collection

            def _details_resolver(product):
                relevant_metadata_ids = session.query(ProductMeta.metadata_id).filter_by(product_id=product.id).all()
                relevant_metas = session.query(Metadata).filter_by(site_id=product.site_id).filter(Metadata.id.in_(relevant_metadata_ids)).all()

                for prod_meta in relevant_metas:
                    # import pdb; pdb.set_trace()
                    details_meta_dict[prod_meta.name] = prod_meta.value
                return details_meta_dict

            def _category_resolver(product):
                prod_category = product.categories.name
                if prod_category not in category_collection:
                    category_collection.append(prod_category)
                return category_collection

            for product in products:
                meta_dict[product.sites.url.split('?')[0]] = {
                    "name": product.name,
                    "categories": _category_resolver(product),
                    "reviews": _review_resolver(product.reviews),
                    "meta_data": _details_resolver(product)
                }
            return meta_dict

        product_name = _product_name_resolver(item_dict.get('product'))
        return _product_meta_resolver(item_dict, product_name)
