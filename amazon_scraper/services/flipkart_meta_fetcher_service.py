from googlesearch import search
import requests
from bs4 import BeautifulSoup
from lxml import etree
import re
from services.google_search_service import GoogleSearch
import logging

logging.basicConfig(filename="Flipkart.log", level=logging.DEBUG,
                    format='%(asctime)s %(message)s',
                    filemode='w')

logger = logging.getLogger()


class FlipkartMetaFetcherService:
    def __init__(self, product_name):
        self.product_name = product_name

    def _image_resolver(self, soup_data):
        images_batch = soup_data.find_all('img', {'src': re.compile('.jpeg')})
        page_images = {}
        for idx, image in enumerate(images_batch):
            page_images[f"image_{idx}"] = image.get('src', '')
        return page_images

    def _flipkart_request_obj(self):
        return requests.get(self.flipkart_url)

    def _product_present_in_flipkart(self):
        gsq = GoogleSearch(self.product_name)
        self.flipkart_url = gsq.google_search()
        return self._valid_flipkart_url()

    def _valid_flipkart_url(self):
        return self.flipkart_url and 'flipkart' in self.flipkart_url

    def _beautiful_soup_obj(self, req_obj):
        return BeautifulSoup(req_obj.text, 'html.parser')

    def _meta_dict_builder(self, page_images, product_rating, product_price):
        flipkart_url = {'flipkart_url': self.flipkart_url}
        return {**page_images, **product_rating, **product_price, **flipkart_url}

    def _dom_obj(self, bfs):
        return etree.HTML(str(bfs))

    def _rating_resolver(self, dom_obj):
        try:
            rating_value = dom_obj.xpath(
                '//*[@id="container"]/div/div[3]/div[1]/div[2]/div[2]/div/div[2]/div/div/span/div[1]')[0].text
        except Exception as e:
            logger.error(
                f"\n\nF6A. Encountered Error While Parsing Rating\n{e}")
        return {'flipkart_rating': rating_value}

    def _price_resolver(self, dom_obj):
        try:
            price_value = dom_obj.xpath(
                '//*[@id="container"]/div/div[3]/div[1]/div[2]/div[2]/div/div[3]/div[1]/div/div[1]')[0].text
        except Exception as e:
            logger.error(
                f"\n\nF7A. Encountered Error While Parsing Price\n{e}")
        return {'flipkart_price': price_value}

    def page_search(self):
        if not self._product_present_in_flipkart():
            logger.warning("\n\nF1. PRODUCT NOT PRESENT IN FLIPKART!")
            return {}

        req_obj = self._flipkart_request_obj()
        logger.warning("\n\nF2. Request Object Created.")
        bfs = self._beautiful_soup_obj(req_obj)
        logger.warning("\n\nF3. BeautifulSoup Object Created.")
        dom_obj = self._dom_obj(bfs)
        logger.warning("\n\nF4. DOM Object Created.")
        page_images = self._image_resolver(bfs)
        logger.warning(f"\n\nF5. Images Fetched.\n Images: {page_images}")
        product_rating = self._rating_resolver(dom_obj)
        logger.warning(f"\n\nF6. Rating Fetched.\n Rating: {product_rating}")
        product_price = self._price_resolver(dom_obj)
        logger.warning(f"\n\nF7. Price Fetched.\n Price: {product_price}")
        flipkart_meta_dict = self._meta_dict_builder(
            page_images, product_rating, product_price)
        logger.warning(f"\n\nF8. Meta Dict Constructed: {flipkart_meta_dict}")
        return flipkart_meta_dict
