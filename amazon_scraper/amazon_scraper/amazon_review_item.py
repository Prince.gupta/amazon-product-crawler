import scrapy

class AmazonReviewItem(scrapy.Item):
    name = scrapy.Field()
    reviewerLink = scrapy.Field()
    reviewTitle = scrapy.Field()
    reviewBody = scrapy.Field()
    verifiedPurchase = scrapy.Field()
    postDate = scrapy.Field()
    starRating = scrapy.Field()
    helpful = scrapy.Field()
    nextPage = scrapy.Field(default="null")
