from sqlalchemy import Column, Integer, Float, String, Boolean, Text, DateTime, JSON,\
    ForeignKey, func, Index, inspect, text, ARRAY
from sqlalchemy import event
from models.base import Base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property

from sqlalchemy.dialects.postgresql import UUID, JSONB
import json
import time


class Review(Base):
    __tablename__ = 'reviews'

    id = Column(UUID(as_uuid=True), primary_key=True,
                server_default=text("uuid_generate_v4()"))
    product_id = Column(UUID(as_uuid=True), ForeignKey('products.id'), nullable=False)
    name = Column(String)
    author_path = Column(Text)
    review_title = Column(Text)
    review = Column(Text)
    verified_purchase = Column(Boolean)
    review_date = Column(DateTime)
    review_rating = Column(Float)
    created_at = Column(DateTime, default=func.now())
    archived_at = Column(DateTime)
    updated_at = Column(DateTime, onupdate=func.now(), default=func.now())
